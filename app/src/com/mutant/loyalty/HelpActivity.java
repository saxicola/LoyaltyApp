package com.mutant.loyalty;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.File;
import java.util.Arrays;
import java.util.Locale;
import android.util.Log;
import java.lang.StringBuilder;



public class HelpActivity extends Activity {

        private WebView webView;

        public void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                Bundle bundle = getIntent().getExtras();
                setContentView(R.layout.webview);
                webView = (WebView) findViewById(R.id.webView1);
                InputStream page = getResources().openRawResource(R.raw.user_manual);
                BufferedReader r = new BufferedReader(new InputStreamReader(page));
                String s;
                StringBuilder builder = new  StringBuilder();
                try
                {
                while ((s = r.readLine()) != null)
                    builder.append(s);
                r.close();
                }
                catch(Exception e)
                {
                Log.d("ERROR",  e.getMessage(), e);
                }
                webView.loadData(builder.toString(), "text/html", "UTF-8");
                //Ensure links open in this webview and not in the default browser
                /*webView.setWebViewClient(new WebViewClient()
                {
                        @Override
                        public boolean shouldOverrideUrlLoading(WebView view, String url)
                        {
                                view.loadUrl(url);
                                return false;
                        }
                });
                */
        }

}
