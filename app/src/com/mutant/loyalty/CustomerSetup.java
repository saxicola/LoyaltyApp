package com.mutant.loyalty;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceActivity;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ListView; // For GUI

import java.io.UnsupportedEncodingException;
import java.io.File;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Formatter;


/************************************************************************
 Ask the user for new or edits to feature name.
*/

public class CustomerSetup extends Activity //PreferenceActivity
{
    String featureName;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    int fixNumber;
    DataStore ds = new DataStore(this);
    //String imagePath = Environment.getExternalStorageDirectory();
    
    Context context;

      @Override
      public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("CustomerSetup","I am in you!");
        Bundle bundle = getIntent().getExtras();
        setContentView(R.layout.customer_dialog); 
       }

   
    /***************************************************************************
    Add a new feature to the set
    */
    public void addCustomer(View view)
    {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Intent customerIntent = new Intent();
        File imagePath = Environment.getExternalStorageDirectory();
        Uri outputFileUri = Uri.fromFile( imagePath);
        cameraIntent.putExtra( MediaStore.EXTRA_OUTPUT, outputFileUri );
        EditText nameFirst = (EditText) findViewById(R.id.edit_message1);
        EditText nameFamily = (EditText) findViewById(R.id.edit_message2);
        //String customerName = nameFirst.getText().toString() + " " + nameSecond.getText().toString();
        customerIntent.putExtra("nameFirst", nameFirst.getText().toString());
        customerIntent.putExtra("nameFamily", nameFamily.getText().toString());
        setResult(RESULT_OK, customerIntent);
        //startActivityForResult(cameraIntent,REQUEST_IMAGE_CAPTURE);
        finish();
    }

    // Deal with camera data, if enabled.
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Uri photoUri = null;
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            //mImageView.setImageBitmap(imageBitmap);
        }
    }

    
    /***************************************************************************
    TODO
    */
    public void getCustomers(View view)
    {
        finish();
    }

    /***************************************************************************
    TODO
    */
    public void cancelMessage(View view)
    {
        finish();
    }

    


} //  END CLASS
