/**
Copyright (C) 2015 Mike Evans <mikee@saxicola.co.uk>

A Loyalty card system for a Pub, or similar, using NFC cards.

Data are stored in a database not on the cards so secure(ish) from customer abuse.
But obviously some form of data backup is required.  Dropbox is a potential storage
option for backups. It's encrypted and secure as long as the password is secured.
Or even I suppose the default google backup thingy.
*/


package com.mutant.loyalty;

import android.app.Activity;
import android.app.backup.BackupManager;
import android.app.backup.RestoreObserver;
import android.app.PendingIntent;
import android.net.Uri;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.graphics.Rect;
import android.graphics.Point;


import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.NfcF;
import android.nfc.tech.MifareClassic;
import android.nfc.tech.MifareUltralight;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.os.SystemClock;
import android.util.Log;
import android.view.Display;
import android.view.HapticFeedbackConstants;
import android.view.View.MeasureSpec;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TableRow;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.File;
import java.util.Arrays;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.lang.StringBuilder;
import java.lang.Runnable;

import java.nio.channels.FileChannel;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;


public class Loyalty extends Activity
{
    public static int MAXCUSTOMERS = Config.MAXCUSTOMERS; // Set to 0 for the paid version or limit users for free version.
    private NdefMessage mNdefPushMessage;
    private TextView mTextView;
    private NfcAdapter mNfcAdapter;
    private PendingIntent mPendingIntent;
    private IntentFilter[] mIntentFilters;
    private String[][] mNFCTechLists;
    DataStore ds = new DataStore(this);
    private boolean waitNewCard = false;
    String lastCID = null;
    String nameFirst = null;
    String nameFamily = null;
    boolean claiming = false;
    boolean rewarding = false;
    MyBackUpAgent myBackupAgent = new MyBackUpAgent();
    private long mLastClickTime = 0;
    private long mLastButtonClickTime = 0; // Time since last button click
    private long mLastScanTime = 0; // Time since card was scanned
    private long clickTimeTimout = 15000; // In ms
    private long buttonTimeOut = 500; // In ms
    //private BackupManager backupManager;
    //private Handler timeoutHandler = new Handler;
    private Timer timer = null;// = new Timer();
    private MyTimerTask myTimerTask;// = new MyTimerTask();


    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        //backupManager = new BackupManager(getBaseContext());
        //backupManager.dataChanged(); // TEST CODE
        // Check and create a files dir, for image storage perhaps?
        String dirPath = getFilesDir().getAbsolutePath();
        Log.d("Files Dir", dirPath);
        File filesDir = new File(dirPath);
        if (!filesDir.exists())
        {
            filesDir.mkdirs();
            File imagesDir = new File(dirPath + File.separator + "images");
            imagesDir.mkdirs();
            imagesDir = new File(dirPath + File.separator + "database");
            imagesDir.mkdirs();
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        createScreenLayout();
        mTextView = (TextView)findViewById(R.id.tv);
        if (mTextView == null)
        {
            Log.e("ERROR","mTextView is null");
            finish();
        }
        mTextView.setHapticFeedbackEnabled(true);
        
        //mTextView.performHapticFeedback(true);
        // Get and test for NFC device
        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (mNfcAdapter != null) {
            mTextView.setText(getString(R.string.Read_Loyalty_Card));
        } else {
            mTextView.setText(getString(R.string.This_device_is_not_NFC_enabled));
        }
        // create an intent with tag data and deliver to this activity
        mPendingIntent = PendingIntent.getActivity(this, 0,
            new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        // set an intent filter for all MIME data
        IntentFilter ndefIntent = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        try
        {
            ndefIntent.addDataType("*/*");
            mIntentFilters = new IntentFilter[] {  ndefIntent  };
            Log.d("Loyalty", "Tag thing.");
        } catch (Exception e)
        {
            Log.d("Loyalty", e.toString());
        }

        mNFCTechLists = new String[][] { new String[] { NfcF.class.getName() } };
        Intent intent = getIntent();
        Log.d("onCreate",(String)intent.getAction());
        if((String)intent.getAction() == "android.nfc.action.NDEF_DISCOVERED") onNewIntent(intent);

    } // END onCreate()
    

    /**************************************************************************
    On any user activity this runs
    */
    @Override
    public void onUserInteraction(){
        Log.d("onUserInteraction","Some sorta user shit happened.");   
    }

    /**************************************************************************
     This happens whenever we scan a tag.
    */
    @Override
    public void onNewIntent(Intent intent) {
        String action = intent.getAction();
        String s;
        //s = NfcAdapter.ACTION_TAG_DISCOVERED;
        //Log.d("ACTION_TAG_DISCOVERED", s);
        // Make the FINISHED button RED
        Button b = (Button)findViewById(R.id.done);
        b.getBackground().setColorFilter(new LightingColorFilter(0x000000, 0xFF0000));
        Log.d("onNewIntent",(String)intent.getAction());
        Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        //intent.putExtra (NfcAdapter.EXTRA_TAG, null);
        intent.removeExtra (NfcAdapter.EXTRA_TAG);
        int points = 0;
        lastCID = bytesToHexString(intent.getByteArrayExtra(NfcAdapter.EXTRA_ID)); // Same as above it seems.
        if(lastCID == null)
        {
            Log.d("NFC Tag", "lastCID is NULL!");
            return;
        }
        Log.d("NFC Tag", lastCID);
        // Look up user in DB
        if(waitNewCard == false)
        {
            Log.d("onNewIntent","Dealing with existing card");
            points = ds.getTotalForCustomerPoints(lastCID);
            s = ds.getFullName(lastCID);
            if(s.equals("No such customer!"))
            {
                lastCID = null;
                mTextView.setText(s);
                return;
            }
            mTextView.setText(s + ". Points=" + Integer.toString(points));
        }
        else if (waitNewCard == true)
        {
            Log.d("onNewIntent","Adding new custome");
            mTextView.setText(getString(R.string.Registered_new_card));
            addCustomer();
            waitNewCard = false;
            lastCID = null;
        }
        if(timer == null)
        {
            timer = new Timer();
            myTimerTask = new MyTimerTask();
            timer.schedule(myTimerTask, clickTimeTimout);
        }
        lastCID = null;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.layout.menu, menu);
        return true;
    }


    @Override
    public void onResume()
    {
        super.onResume();

        if (mNfcAdapter != null)
            mNfcAdapter.enableForegroundDispatch(this, mPendingIntent, mIntentFilters, mNFCTechLists);
    }


    @Override
    public void onPause()
    {
        super.onPause();
        if (mNfcAdapter != null)
            mNfcAdapter.disableForegroundDispatch(this);
    }


    private static Point getDisplaySize(final Display display)
    {
        final Point point = new Point();
        try {
            display.getSize(point);
        } catch (java.lang.NoSuchMethodError ignore) { // Older device
            point.x = display.getWidth();
            point.y = display.getHeight();
        }
        return point;
    }

    /*************************************************************************
    Dynamically create or update the screen layout.
    Use the rewards table as source data.
    */
    private void createScreenLayout()
    {
        final Display display = getWindowManager().getDefaultDisplay();
        Point size; // = new Point();
        size =  getDisplaySize(display);


        int width = size.x;
        int height = size.y;
        Cursor cursor = ds.getRewards();
        View scrollView = (View) findViewById(R.id.hScrollView1);
        Rect windowRect = new Rect();
        scrollView.getWindowVisibleDisplayFrame(windowRect);

        int hWindow = windowRect.bottom - windowRect.top;
        int wWindow = windowRect.right - windowRect.left;
        TableLayout tl = (TableLayout) findViewById(R.id.input2);
        TableRow tr = null;
        int counter=0;
        while (cursor.moveToNext())
        {
            int rid = cursor.getInt(cursor.getColumnIndex("rid"));
            if(counter % 3 == 0 )
            {
                //Create new row
                tr = new TableRow(this);
                tr.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.WRAP_CONTENT,1));
            }
            final Button b = new Button(this);
            b.setText(cursor.getString(cursor.getColumnIndex("product")));
            b.setId(rid);
            b.setTextSize(30);
            
            b.setOnClickListener(new View.OnClickListener(){
                public void onClick(View v) {
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 200)
                    {
                        //b.getBackground().setColorFilter(new LightingColorFilter(0x000000, 0xE81313));
                        return;
                    }
                    mLastClickTime = SystemClock.elapsedRealtime();
                    productClick(v);
                    }});
            TableRow.LayoutParams params = new TableRow.LayoutParams(10, (int)(height/8));
            params.setMargins(0, 20, 10, 0);
            b.setLayoutParams(params);
            tr.addView(b);
            if(counter % 3 == 0)
            {
                tl.addView(tr, new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                        TableLayout.LayoutParams.WRAP_CONTENT));
            }
            counter += 1;
        }
        cursor.close();

    }


    /*******************************************************
    Deal with menu item selection.
    */
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        String result = "";
        Intent myIntent; // intent declaration
        switch (item.getItemId())
        {
            case R.id.rewards: // Edit the rewards|product list
                myIntent = new Intent(this.getApplicationContext(), Password.class);
                startActivityForResult(myIntent,3);
                break;

            case R.id.menu_new: // Add a new Customer
                myIntent = new Intent(this.getApplicationContext(), Password.class);
                startActivityForResult(myIntent,4);
                break;

            case R.id.menu_export: // Export data as CSV
                result = ds.exportAllCSV();
                Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
                break;

            case R.id.menu_export_sales: // Export data as CSV
                myIntent = new Intent(this.getApplicationContext(), Password.class);

                startActivityForResult(myIntent,5);
                break;

            case R.id.menu_export_database: // Export data as CSV
                myIntent = new Intent(this.getApplicationContext(), Password.class);
                startActivityForResult(myIntent,6);
                break;

            case R.id.menu_open_manual: // Open the help manual
                myIntent = new Intent(this.getApplicationContext(), HelpActivity.class);
                startActivity(myIntent);
                break;

            case R.id.prefs: //
                if(true){return true;} // TODO
                myIntent = new Intent(this.getApplicationContext(), Password.class);
                startActivityForResult(myIntent,7);
                break;

        }
        return true;
    }


    /***************************************************************************
    */
    protected void onActivityResult (int requestCode, int resultCode, Intent data)
    {
        Intent myIntent;
        String password = null;
        String result = null;
        if (data == null) return;
        Log.d("DEBUG: onActivityResult, requestCode =",Integer.toString(requestCode));
        switch (requestCode)
        {
            case 1: // return from CustomerSetup.
                try
                {
                    nameFirst = data.getStringExtra("nameFirst");
                    nameFamily = data.getStringExtra("nameFamily");
                    Log.d("DEBUG: onActivityResult",nameFirst + " " + nameFamily);
                    mTextView.setText("Now scan the new Loyalty Card or any button to cancel action.");
                    // FIXME:  How do we know we have the latest scan?
                    waitNewCard = true;
                }
                catch(Exception e)
                {
                    // User probably cancelled.
                    Log.d("onActivityResult", "error -- " + e.getMessage(), e);
                }
                break;

            case 2:  // Return from  RewardSetup
                // remove the old and update a new display.
                Log.d("DEBUG: onActivityResult","Rebulding main screen");
                TableLayout layout  = (TableLayout) findViewById(R.id.input2);
                for (int i = layout.getChildCount() - 1; i > 0; i--)
                {
                    View child = layout.getChildAt(i);
                    if (child instanceof TableRow)
                    {
                        layout.removeView(child);
                    }
                }
                createScreenLayout();
                break;

            case 3: // This is a return from Password for RewardSetup

                myIntent = new Intent(this.getApplicationContext(), RewardSetup.class);
                password = data.getStringExtra("password");
                Log.d("case3:",password);
                if(password.equals("OK"))
                    startActivityForResult(myIntent,2);
                break;

            case 4:// This is a return from Password for CustomerSetup
                myIntent = new Intent(this.getApplicationContext(), CustomerSetup.class);
                password = data.getStringExtra("password");
                Log.d("case4:",password);
                if(password.equals("OK"))
                    startActivityForResult(myIntent,1);
                break;

            case 5:
                    password = data.getStringExtra("password");
                    if(password.equals("OK"))
                    {
                        result = ds.exportSales();
                        Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
                    }
                break;

            case 6:
                password = data.getStringExtra("password");
                if(password.equals("OK"))
                {
                    result = ds.exportDatabase();
                    Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
                }
                break;

            case 7:
               password = data.getStringExtra("password");
                if(password.equals("OK"))
                {
                    myIntent = new Intent(this.getApplicationContext(), SettingsActivity.class);
                    startActivityForResult(myIntent,8);
                }
            break;

        }
    }


    /**********************************************************************

    */
    private void addCustomer()
    {
        Log.d("addCustomer: Cust count",Integer.toString( ds.getCustomerCount()));
        if(MAXCUSTOMERS !=0  && ds.getCustomerCount() >= MAXCUSTOMERS)
        {
            Toast.makeText(this, "Maximum customers exceeded. Upgrade to paid version.", Toast.LENGTH_SHORT).show();
            return;
        }
        String result = ds.addCustomer(nameFirst, nameFamily, lastCID);
        Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
    }

    /**********************************************************************
    Deal with product clicked button
    */
    public void productClick(View v)
    {
        
        //v.performHapticFeedback(HapticFeedbackConstants.CONTEXT_CLICK);
        if(waitNewCard)
        {
            waitNewCard = false;
            mTextView.setText("Action cancelled. Read Loyalty Card");
            return;
        }
        if(v.getId() == 0 ) finished(v);
        if(v.getId() == R.id.add) return;
        else if(lastCID != null)
        {
            // Update the rewards points
            String product = null;
            Button b = (Button)v;
            b.setEnabled(false);
            product = b.getText().toString();
            //Log.d("buttonClick product",product);

            if (claiming)
            {
                //v.playSoundEffect(android.view.SoundEffectConstants.CLICK);
                int points = ds.getRedeemPoints(v.getId());
                int prodID = b.getId(); // = rewards.rid
                int rp = ds.redeemPoints(prodID, lastCID);
                if (rp == 0)
                {
                    mTextView.setText("Not enough points!");
                    b.setEnabled(true);
                    b = (Button)findViewById(R.id.claim);
                    claiming = false;
                    return;
                }
                int total = ds.getTotalForCustomerPoints(lastCID);
                mTextView.setText("Redeemed " + product + ": " + points + ".  Total Points = " +
                                    Integer.toString(total));
            }
            else if(rewarding)
            {
                //v.playSoundEffect(android.view.SoundEffectConstants.CLICK);
                int prodID = b.getId(); // = rewards.rid
                int points = ds.getRewardPoints(v.getId());
                int total = ds.addCustomerPoints(lastCID, prodID); // TODO use rewards.rid
                mTextView.setText("One " + product + ": " + points + ":  Total Points = " + Integer.toString(total));
                b.setEnabled(true);
            }
            b.setEnabled(true);
        }
        if(timer != null) //reset the timer
        {
            timer.cancel();
            timer = new Timer();
            myTimerTask = new MyTimerTask();
            timer.schedule(myTimerTask, clickTimeTimout);
        }
    }


    /**********************************************************************
    CLAIM button pressed
    */
    public void claimLoyalty(View v)
    {
        if (lastCID == null) return;
        v.playSoundEffect(android.view.SoundEffectConstants.CLICK);
        claiming = true;
        rewarding = false;
        Button b = (Button)findViewById(R.id.add);
        b.getBackground().setColorFilter(new LightingColorFilter(0x000000, 0xE5E5E5));
        b = (Button)v;
        b.getBackground().setColorFilter(new LightingColorFilter(0x000000, 0xFFA500));

        if(timer != null) //reset the timer
        {
            timer.cancel();
            timer = new Timer();
            myTimerTask = new MyTimerTask();
            timer.schedule(myTimerTask, clickTimeTimout);
        }
    }

    
    /**********************************************************************
    ADD button pressed
    */
    public void addLoyalty(View v)
    {
        if (lastCID == null) return;
        v.playSoundEffect(android.view.SoundEffectConstants.CLICK);
        rewarding = true;
        claiming = false;
        Button b = (Button)findViewById(R.id.claim);
        b.getBackground().setColorFilter(new LightingColorFilter(0x000000, 0xE5E5E5));
        b = (Button)v;
        b.getBackground().setColorFilter(new LightingColorFilter(0x000000, 0xFFA500));
        if(timer != null) //reset the timer
        {
            timer.cancel();
            timer = new Timer();
            myTimerTask = new MyTimerTask();
            timer.schedule(myTimerTask, clickTimeTimout);
        }
    }


    /**********************************************************************
    FINISHED button pressed
    */
     public void finished(View view)
    {
        Button b = (Button)findViewById(R.id.add);
        b.getBackground().setColorFilter(new LightingColorFilter(0x000000, 0xE5E5E5));
        b = (Button)findViewById(R.id.claim);
        b.getBackground().setColorFilter(new LightingColorFilter(0x000000, 0xE5E5E5));
        b = (Button)findViewById(R.id.done);
        b.getBackground().setColorFilter(new LightingColorFilter(0x000000, 0xE5E5E5));
        b = (Button)findViewById(R.id.add);
        b.getBackground().setColorFilter(new LightingColorFilter(0x000000, 0xE5E5E5));
        b = (Button)findViewById(R.id.claim);
        b.getBackground().setColorFilter(new LightingColorFilter(0x000000, 0xE5E5E5));
        lastCID = null;
        claiming = false;
        rewarding = false;
        if(timer != null)
        {
            timer.cancel();
            timer = null;
        }
        mTextView.setText("Read Loyalty Card");
    }


    /**********************************************************************
    Deal with numeric entry buttons.
    */
    public void numberClick(View v)
    {
        switch (v.getId()) {
            case 1:
                Log.d("numberClick","Button 1 clicked");
                break;
           /* case R.id.button_two:
                // do something else
                break;
            case R.id.button_three:
                // i'm lazy, do nothing
                break;*/
        }
    }


    /***********************************************************************
     Utility function
    */
    private static String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder("0x");
        if (src == null || src.length <= 0) {
            return null;
        }

        char[] buffer = new char[2];
        for (int i = 0; i < src.length; i++) {
            buffer[0] = Character.forDigit((src[i] >>> 4) & 0x0F, 16);
            buffer[1] = Character.forDigit(src[i] & 0x0F, 16);
            System.out.println(buffer);
            stringBuilder.append(buffer);
        }

        return stringBuilder.toString();
    }

    
    class MyTimerTask extends TimerTask {

      @Override
      public void run() {
       
       
       runOnUiThread(new Runnable(){

        @Override
        public void run() {
          finished(null);
        }});
      }
      
     }

} // END CLASS
