package com.mutant.loyalty;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import android.database.Cursor;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceActivity;
import android.text.InputType;
import android.util.Log;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView; // For GUI
import android.widget.TableRow;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;



public class Password extends Activity
{
    private TextView mTextView;
    DataStore ds = new DataStore(this);
    int mode = 0;
    String p1;
    String p2;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
    super.onCreate(savedInstanceState);
    Log.d("Password","I am in you!");
    Bundle bundle = getIntent().getExtras();
    setContentView(R.layout.passwd_dialog);
    }


    public void processPasswd(View v)
    {
        EditText passwd = null;
        //try {
            passwd = (EditText) findViewById(R.id.txtPassword);
        //    }
        //catch(Exception e){}
        String pass = passwd.getText().toString();
        if(pass.equals("password")) mode = 1;
        if(mode != 0)
        {
            changePassword(v);
            Log.d("processPasswd: ", "Returning.");
        }
        else validatePasswd(v);

    }
/**************************************************************************
    Input admin password
    TODO: Set password and check password.
    */
    public void validatePasswd(View v)
    {
        Intent intent = new Intent();
        EditText passwd = (EditText) findViewById(R.id.txtPassword);
        if(passwd == null)Log.d("validatePasswd","Null Pointer");
        String pass = passwd.getText().toString();
        // Hash and test passwd.
        String phash = ds.getPasswordHash("admin");
        //Log.d("validatePasswd: passhash",phash);
        //Log.d("validatePasswd: enc pass", ds.hashPassword(pass));
        String test = String.valueOf(phash.equals(ds.hashPassword(pass)));
        Log.d("validatePasswd: test",test);
        if (phash.equals(ds.hashPassword("password"))) // Default password still set
        {
            //Log.d("validatePasswd: passhash",ds.hashPassword(pass));
            // Default password set, so set it to whatever was typed in.
            ds.setPasswordHash("admin", ds.hashPassword(pass));
            Toast.makeText(this, "New password has been set.", Toast.LENGTH_SHORT).show();
            Toast.makeText(this, "Now press SUBMIT again.", Toast.LENGTH_SHORT).show();
            //finish();
        }
        else if(phash.equals(ds.hashPassword(pass)))
        {
            intent.putExtra("password", "OK");
            setResult(RESULT_OK, intent);
            Log.d("validatePasswd: enc pass", "OK");
            finish();
        }
        else
        {
            intent.putExtra("password", "NO");
            setResult(RESULT_OK, intent);
            Log.d("validatePasswd: enc pass", "NO");
            finish();
        }
        //finish();
    }


    /**************************************************************************
    Set a new password, checking the new password entry is accurate.
    */
    public void changePassword(View v)
    {

        mTextView = (TextView)findViewById(R.id.lblPassword);
        EditText passwd = (EditText) findViewById(R.id.txtPassword);
        String pass = passwd.getText().toString();
        String phash = ds.getPasswordHash("admin");

        switch(mode)
        {
            case 1:
                mode = 2;
                mTextView.setText("Confirm old password.");
                Log.d("changePasswd: mode = ", Integer.toString(mode));
                passwd.setText("");
                break;

            case 2:
                Log.d("changePasswd: mode = ", Integer.toString(mode));
                if(phash.equals(ds.hashPassword(pass)))
                {
                    mTextView.setText("Input new password.");
                    passwd.setText("");
                    mode = 3;
                }
                else
                {
                    mode = 0;
                    mTextView.setText("Password");
                    passwd.setText("");
                    Toast.makeText(this, "Password fail.", Toast.LENGTH_SHORT).show();
                }
                break;

            case 3:
                mode = 4;
                p1 = pass;
                mTextView.setText("And again please.");
                passwd.setText("");
                Log.d("changePasswd: mode = ", Integer.toString(mode));
                break;

            case 4:
                p2 =  pass;
                if(p1.equals(p2) && !pass.equals("password")) // Set new passwd
                {
                    ds.setPasswordHash("admin", ds.hashPassword(pass));
                    mTextView.setText("Password");
                    Toast.makeText(this, "Your new password is set.", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(this, "Your passwords differ.", Toast.LENGTH_SHORT).show();
                }
                passwd.setText("");
                mode = 0;
                Log.d("changePasswd: mode = ", Integer.toString(mode));
                break;
        }
    }

} // END CLASS




