package com.mutant.loyalty;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceActivity;
import android.text.InputType;
import android.util.Log;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView; // For GUI
import android.widget.TableRow;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.Float;
import java.lang.Double;
import java.lang.Long;
import java.lang.Integer;
import java.util.ArrayList;
import java.util.List;


public class RewardSetup extends Activity
{
    DataStore ds = new DataStore(this);


    @Override
    public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Log.d("Reward Setup","I am in you!");
    Bundle bundle = getIntent().getExtras();
    setContentView(R.layout.reward_dialog);
    createLayout();
    }


    private void createLayout()
    {
        Cursor cursor = ds.getRewards();
        TableLayout tl = (TableLayout) findViewById(R.id.product_setup);
        TableRow tr = null;
        int counter=0;
        while (cursor.moveToNext())
        {
            addRow(cursor);
        }

        addRow((Cursor)null);
        return;
    }

    private void addRow(Cursor cursor)
    {
        TableLayout tl = (TableLayout) findViewById(R.id.product_setup);
        TableRow tr = new TableRow(this);
        tr.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
        EditText editOrdering = new EditText(this);
        EditText editProduct = new EditText(this);
        EditText editReward = new EditText(this);
        EditText editRedeem = new EditText(this);
        editOrdering.setMinEms(2);
        editProduct.setMinEms(2);//setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,20,2));
        editReward.setMinEms(2);//setLayoutParams(new LayoutParams(30,20,1));
        editRedeem.setMinEms(2);//setLayoutParams(new LayoutParams(30,20,1));
        editOrdering.setHint("Ord");
        editProduct.setHint("Product");
        editReward.setHint("Earn");
        editRedeem.setHint("Cost");
        if(cursor != null)
        {
            int order = cursor.getInt(cursor.getColumnIndex("ordering"));
            String product = cursor.getString(cursor.getColumnIndex("product"));
            int reward = cursor.getInt(cursor.getColumnIndex("reward"));
            int redeem = cursor.getInt(cursor.getColumnIndex("redeem"));
            editOrdering.setText(Integer.toString(order));
            editProduct.setText(product);
            editReward.setText(Integer.toString(reward));
            editRedeem.setText(Integer.toString(redeem));
        }
        editOrdering.setInputType(InputType.TYPE_CLASS_NUMBER);
        editProduct.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        editReward.setInputType(InputType.TYPE_CLASS_NUMBER);
        editRedeem.setInputType(InputType.TYPE_CLASS_NUMBER);
        tr.addView(editOrdering);
        tr.addView(editProduct);
        tr.addView(editReward);
        tr.addView(editRedeem);
        Button b = new Button(this);
        if(cursor == null)
        {
            b.setText("ADD");
            b.setOnClickListener(new View.OnClickListener(){
                public void onClick(View v) {addProduct();}});
        }
        else
        {
            b.setText("DEL");
            int rid = cursor.getInt(cursor.getColumnIndex("rid"));
            b.setId(rid);
            b.setOnClickListener(new View.OnClickListener(){
                public void onClick(View v) {deleteProduct(v);}});
        }
        tr.addView(b);
        tl.addView(tr, new TableLayout.LayoutParams(TableLayout.LayoutParams.FILL_PARENT,
                    TableLayout.LayoutParams.WRAP_CONTENT));
         if(cursor == null)
        {

            tr = new TableRow(this);
            b = new Button(this);
            b.setText("DONE");
            b.setOnClickListener(new View.OnClickListener(){
                    public void onClick(View v) {done(v);}});
            tr.addView(b);
            tl.addView(tr, new TableLayout.LayoutParams(TableLayout.LayoutParams.FILL_PARENT,
                        TableLayout.LayoutParams.WRAP_CONTENT));
        }
    }

    public void deleteProduct(View v)
    {
        Log.d("deleteProduct",Integer.toString(v.getId()));
        TableLayout layout  = (TableLayout) findViewById(R.id.product_setup);
        ds.deleteProduct(v.getId());
        Button b = (Button)layout.findViewById(v.getId());
        // Update layout?
        TableRow tr = (TableRow) b.getParent();
        layout.removeView(tr);
        b.setText("---");

    }

    public void addProduct()
    {
        // Get the last row data and add it to the database
        TableLayout tl = (TableLayout) findViewById(R.id.product_setup);
        int rows = tl.getChildCount();
        View addy  = tl.getChildAt(rows-2);
        if (addy instanceof TableRow)
        {
                TableRow row = (TableRow) addy;
                EditText editOrdering = (EditText) row.getChildAt(0);
                EditText editProduct = (EditText) row.getChildAt(1);
                EditText editReward = (EditText) row.getChildAt(2);
                EditText editRedeem = (EditText) row.getChildAt(3);
                try
                {
                    int redeem = 0;
                    int reward = 0;
                    int  ordering = 999;
                    try{ordering = Integer.parseInt(editOrdering.getText().toString());}
                    catch(NumberFormatException nfe){ordering = 999;}
                    String product = editProduct.getText().toString();
                    if(editReward.getText().toString() != ""){reward = Integer.parseInt(editReward.getText().toString());}
                    if(editRedeem.getText().toString() != ""){redeem = Integer.parseInt(editRedeem.getText().toString());}
                    int rid = ds.addReward(product, reward, redeem, ordering);
                    Button b = (Button)row.getChildAt(4); // Edit the Add button
                    b.setText("DEL");
                    b.setId(rid);
                    b.setOnClickListener(new View.OnClickListener(){
                    public void onClick(View v) {deleteProduct(v);}});
                }
                catch(Exception e) // Catch alls are BAD.
                {
                    Log.d("addProduct",e.toString());
                }
                // The DONE button is in the bottom row now, remove it.
                row = (TableRow) tl.getChildAt(rows-1); // Last row
                Button b = (Button)row.getChildAt(0); // Remove the DONE button
                row.removeView(b);
                tl.removeView(row);

        }
        addRow((Cursor)null);
    }// Add a product

    public void done(View v)
    {
         Intent rewardIntent = new Intent();
        // Update any edits in the table.
        TableLayout layout  = (TableLayout) findViewById(R.id.product_setup);
        for (int i = 0; i < layout.getChildCount()-2; i++)
        {
            View child = layout.getChildAt(i);
            if (child instanceof TableRow)
            {
                TableRow row = (TableRow) child;
                for (int x = 0; x < row.getChildCount(); x++)
                {
                    int reward = 0;int redeem = 0;int ordering = 999;
                    EditText editOrdering = (EditText) row.getChildAt(0);
                    EditText editProduct = (EditText) row.getChildAt(1);
                    EditText editReward = (EditText) row.getChildAt(2);
                    EditText editRedeem = (EditText) row.getChildAt(3);
                    Button delB = (Button) row.getChildAt(4); // The button ID is the reward ID
                    try{ordering = Integer.parseInt(editOrdering.getText().toString());}
                    catch(NumberFormatException nfe){ordering = 999;}
                    String product = (String)editProduct.getText().toString();
                    try{reward =  Integer.parseInt(editReward.getText().toString());}
                    catch(NumberFormatException nfe){reward = 0;}
                    try{redeem =  Integer.parseInt(editRedeem.getText().toString());}
                    catch(NumberFormatException nfe){redeem = 0;}
                    int rid =  (delB.getId());
                    // Now update the DB
                    ds.updateProduct(ordering, product, reward, redeem, rid);
                }
            }
        }
        rewardIntent.putExtra("rewrds", "CHANGED"); // Something, anything.
        setResult(RESULT_OK, rewardIntent);
        finish();
    }

    public void editRewards()
    {
    }

} // END CLASS
