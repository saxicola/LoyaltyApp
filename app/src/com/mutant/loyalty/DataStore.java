/*
 * DataStore.java
 *
 * Copyright 2015 Mike Evans <mikee@saxicola.co.uk>
 *
Notes:
The user names are stored obfusticated with base64 encoding, not encrypted.
The potential value of the names is, in my opinion, limited and the data stored
are not of great value to, say, a competitor. The decision to use bsa64 was merely to
provide some degree of hiding from trivial exmination without adding masses of
complication and 'proper' encryption, which in any case needs a key and if that key
is stored in the code then a hacker can easily gain assess to the data with it.

I can be pursuaded otherwise however.

So why did I encrypt the password?  It isn't, it's hashed, comparing hashes is
easy and cannot be applied to user
See http://blog.jgc.org/2012/06/one-way-to-fix-your-rubbish-password.html
for how to add salting without having the user ever know about it.
 *
 */
package com.mutant.loyalty;

import android.content.Context;

import android.app.backup.BackupManager;
import android.app.backup.RestoreObserver;
import android.app.backup.RestoreObserver;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteDoneException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import android.content.ContentValues;

import org.mindrot.jbcrypt.BCrypt;

import android.os.Environment;
import android.text.format.Time;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.opencsv.CSVWriter;
import com.opencsv.*;
import java.sql.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Formatter;

import java.io.File;
import java.io.FileWriter;

import java.nio.channels.FileChannel;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.lang.Float;
import java.lang.Double;
import java.lang.Long;
import java.lang.Integer;

import java.nio.charset.Charset;
import java.security.AlgorithmParameters;
import java.security.SecureRandom;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class DataStore extends SQLiteOpenHelper
{
    public static final String  DATABASE_NAME = "loyalty.db";
    private static final int DATABASE_VERSION = 5;
    private SQLiteDatabase database;// = null;
    private Context context;
    Time now = new Time();
    int secretKey = 847;
    private BackupManager backupManager;
    static final Object[] sDataLock = new Object[0];
    String salt = BCrypt.gensalt();
    String pass = BCrypt.hashpw("password", salt);


    /**
    Constructors.
    */
    public DataStore(Context context)
    {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        backupManager = new BackupManager(this.context);
    }


    // http://www.java2s.com/Code/Android/Core-Class/Backuprestore.htm
    // FIXME backupManager malrky doesn't work
    @Override
    public void onCreate(SQLiteDatabase database)
    {
        /*
        final SQLiteDatabase db = database;
        int result = backupManager.requestRestore(
                new RestoreObserver()
                {
                    public void restoreFinished(int error)
                    {
                        Log.d("DataStore", "Restore finished." + Integer.toString(error));
                        int oldVersion = db.getVersion();
                        Log.d("onCreate",Integer.toString(oldVersion));
                        onUpgrade(db, oldVersion, DATABASE_VERSION);
                    }
                }
        );
        if (result == 0)
        {
            int oldVersion = database.getVersion();
            Log.d("onCreate",Integer.toString(oldVersion));
            onUpgrade(database, oldVersion, DATABASE_VERSION);

        }*/
        if (restoreDB(database)) return; // Old trusted method.
        // If it doesn't exist then...
        else
            createDatabase(database);

    }

/*******************************************************************************
 * Copy backed up datebase to the current database.
 * Useful when upgrading, probably
 */
private boolean restoreDB(SQLiteDatabase database)
  {
    try{
        Log.d("restoreDatabase()","Doing it.");
        File sd = Environment.getExternalStorageDirectory();
        File data = Environment.getDataDirectory();

        if (sd.canRead())
        {
            Log.d("restoreDatabase()","Restoring it.");
            String restroredDBPath = "//data//com.mutant.loyalty//databases//loyalty.db";
            String backupDBPath = "//sdcard//loyalty.db";
            File restoredDB = new File(data, restroredDBPath);
            File backupedDB = new File(backupDBPath);
            if(backupedDB.exists())
            {
                database.close(); // Close the current/new DB
                Log.d("restoredDB", restoredDB.toString());
                Log.d("backupedDB", backupedDB.toString());
                FileChannel src = new FileInputStream(backupedDB).getChannel();
                FileChannel dst = new FileOutputStream(restoredDB).getChannel();
                src.transferTo(0, src.size(), dst);
                src.close();
                dst.close();
            }
            else return false; // No backup exists.
        }
        } catch (Exception e) {
            Log.d("restoreDatabase", e.getMessage());
            //Toast.makeText(getBaseContext(), e.toString(), Toast.LENGTH_LONG).show();
        }
        return true; // Backup restored, phew!
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion)
    {
        //exportDatabase(); // Save it to sdcard Don't do this. It may be empty.
        //restoreDB(database); // Restore from the SD card. assuming a backup exists.
        // NOTE: No breaks this code is designed to fall through.
        switch(oldVersion)
        {
            case 1:
                database.execSQL("ALTER TABLE  redemptions RENAME TO drop_me");
                database.execSQL("CREATE TABLE IF NOT EXISTS redemptions(rid INTEGER PRIMARY KEY AUTOINCREMENT, customer_id TEXT, product_id INTEGER, timestamp DATETIME DEFAULT CURRENT_TIMESTAMP , points INTEGER )");
                database.execSQL("INSERT INTO redemptions(rid,  customer_id, timestamp, points) SELECT rid,  customer_id, timestamp, points FROM drop_me");
                database.execSQL("DROP TABLE drop_me");
            case 2:
            case 3:
                database.execSQL("CREATE TABLE IF NOT EXISTS key_value(key TEXT, value TEXT, value_type TEXT)");
                database.execSQL("INSERT INTO key_value(key, value, value_type) VALUES('expiredays','100','INTEGER')");
            case 4:
                database.execSQL("ALTER TABLE key_value RENAME TO drop_me");
                database.execSQL("CREATE TABLE IF NOT EXISTS key_value(key TEXT, value TEXT, value_type TEXT)");
                database.execSQL("INSERT INTO key_value(key, value) SELECT key, value FROM drop_me");
                database.execSQL("UPDATE key_value SET value_type = 'INTEGER'");
                database.execSQL("DROP TABLE drop_me");
                try{database.execSQL("ALTER TABLE customers ADD COLUMN contact TEXT");}
                catch(Exception e){Log.d("onUpgrade", e.toString());}
                database.execSQL("CREATE TABLE IF NOT EXISTS update_rewards (rew_id INTEGER NOT NULL ,rew_old TEXT NOT NULL ,rew_new CHAR NOT NULL ,date_time DATETIME NOT NULL  DEFAULT (CURRENT_TIMESTAMP) );");
                database.execSQL("CREATE TRIGGER IF NOT EXISTS trig_update_rewards AFTER UPDATE ON rewards BEGIN INSERT into update_rewards (rew_id, rew_old, rew_new) VALUES(new.rid, old.product, new.product); END;");
                try{database.execSQL("ALTER TABLE password ADD COLUMN salt TEXT");}
                catch(Exception e){Log.d("onUpgrade", e.toString());}
            case 5:
                try
                {
                    database.execSQL("ALTER TABLE points ADD COLUMN redeemed INTEGER DEFAULT 0");
                    database.execSQL("UPDATE points SET redeemed = points");
                }
                catch(Exception e){Log.d("onUpgrade", e.toString());}
            case 6:
        }
    }


    @Override
    public void onOpen(SQLiteDatabase db)
    {}


    public void createDatabase(SQLiteDatabase database)
    {
        Log.d("createDatabase","Creating new tables.");
        database.execSQL("CREATE TABLE IF NOT EXISTS customers(cid TEXT PRIMARY KEY, timestamp DATETIME DEFAULT CURRENT_TIMESTAMP, name_first TEXT, name_second TEXT, contact TEXT)");
        database.execSQL("CREATE TABLE IF NOT EXISTS points(pid INTEGER PRIMARY KEY AUTOINCREMENT, timestamp DATETIME DEFAULT CURRENT_TIMESTAMP, cust_key TEXT, points INTEGER, reward_fk INTEGER, redeemed INTEGER DEFAULT 0)");
        database.execSQL("CREATE TABLE IF NOT EXISTS redemptions(rid INTEGER PRIMARY KEY AUTOINCREMENT, customer_id TEXT,  product_id TEXT, timestamp DATETIME DEFAULT CURRENT_TIMESTAMP , points INTEGER )");
        database.execSQL("CREATE TABLE IF NOT EXISTS rewards(rid INTEGER PRIMARY KEY AUTOINCREMENT, ordering INTEGER, product TEXT, reward INTEGER, redeem INTEGER, valid INTEGER DEFAULT 1);");
        database.execSQL("CREATE TABLE IF NOT EXISTS password(pid INTEGER PRIMARY KEY, username TEXT, salt TEXT, passhash TEXT)");
        database.execSQL("CREATE TABLE IF NOT EXISTS mugshots(mid TEXT, image_lnk TEXT)"); // mid is fk to customers.cid, image_lnk points to image path.
        database.execSQL("CREATE TABLE IF NOT EXISTS key_value(key TEXT, value TEXT, value_type TEXT)"); // Use to store other misc config values.
        database.execSQL("CREATE TABLE IF NOT EXISTS update_rewards (rew_id INTEGER NOT NULL ,rew_old TEXT NOT NULL ,rew_new CHAR NOT NULL ,date_time DATETIME NOT NULL  DEFAULT (CURRENT_TIMESTAMP) );");
        database.execSQL("CREATE TRIGGER IF NOT EXISTS trig_update_rewards AFTER UPDATE ON rewards BEGIN INSERT into update_rewards (rew_id, rew_old, rew_new) VALUES(new.rid, old.product, new.product); END;");
        try
        {
            String sql = "INSERT INTO password(pid, username ,passhash) VALUES(1,'admin',?)"; //Set a default empty password.
            SQLiteStatement stmt = database.compileStatement(sql);
            stmt.bindString(1, hashPassword(""));
            stmt.executeInsert();
            database.execSQL("INSERT INTO key_value(key, value, value_type) VALUES('expiredays','100','INTEGER')");
            database.execSQL("INSERT INTO key_value(key, value, value_type) VALUES('gui_timout','15000','INTEGER')");
        } // This will fail if a password is already set so...
        catch(Exception e){Log.d(" createDatabase()", e.toString());}
    }


    /**
    * Add a new customer.
    * @param String First mane of customer.
    * @param String Second/family name of customer.
    * @param String Card number.
    */
    public String addCustomer(String name_first, String name_second, String cardString)
    {
        if (Loyalty.MAXCUSTOMERS != 0 )
        {
            int custCount = getCustomerCount();
            Log.d("DataStore.custCount",Integer.toString(custCount));
            if (custCount >= Loyalty.MAXCUSTOMERS) return "Upgrade to paid version for more customers.";
        }
        database = this.getWritableDatabase();
        now.setToNow();
        String eFirst = tEncrypt(name_first);
        String eSecond = tEncrypt(name_second);
        String sql = "INSERT INTO customers (name_first, name_second, cid) VALUES(?,?,?);";
        SQLiteStatement stmt = database.compileStatement(sql);
        stmt.bindString(1, eFirst);
        stmt.bindString(2, eSecond);
        stmt.bindString(3, cardString);
        try // If card exists do this.  We could just update the user name here but his entails user ID checking and ascertaining if the old user want their card transferred.
        {
            stmt.executeInsert();
        }
        catch(Exception e)
        {
            return "New card not added. Possible duplicate?";
        }
        exportDatabase();
        backupManager.dataChanged();
        return "New card added successfully";
    }

    public void editCustomer(String name)
    {

    }
    
    /**
    * Add a reward to the database.
    * @param String Reward or product name.
    * @param int Number pf points awarded on purchase.
    * @param int Number of points to redeem this reward.
    * @param int The display position of the product.
    */
    public int addReward(String product, int reward, int redeem, int ordering)
    {
        database = this.getWritableDatabase();
        boolean success = true;
        database.beginTransaction(); // This will roll back if success is not set before 
        String sql = "INSERT INTO rewards(product,reward,redeem, ordering) VALUES(?,?,?,?)";
        SQLiteStatement stmt = database.compileStatement(sql);
        stmt.bindString(1, product);
        stmt.bindDouble(2, reward);
        stmt.bindDouble(3, redeem);
        stmt.bindDouble(4, ordering);
        stmt.executeInsert();
        Cursor cursor = database.rawQuery("SELECT MAX(rid) FROM rewards", null);
        if (cursor != null)
        {
            if (cursor.moveToFirst())
            {
                cursor.getInt(cursor.getColumnIndex("MAX(rid)"));
            }
        }
        // Now it gets complicated. :)
        // Update the points (sales) table setting redeemed = some_value so that 
        // the sum of some_values = reward_points and are < points in each case.
        // We don't want to load loads of tuples into a list.list we want to do
        // it the DBA way.
        
        if(success == true)
        {
            database.setTransactionSuccessful();
        }
        else
        {
            Log.d("addCustomerPoints","Failed to add points.");
        }
        database.endTransaction();
        exportDatabase();
        if(success == true){return reward;}
        else {return 0;}
    }


    /**
    * Retrieve the customer name using the card number.
    * @param String Card ID
    * @return String Customer name as: First_name Last_name
    */
    public String getFullName(String cid)
    {
        database = this.getWritableDatabase();
        Log.d("getFullName","Trying to match: " + cid);
        String result = "";
        String table = "customers";
        String[] columnsToReturn = { "name_first", "name_second"};
        String selection = "cid = ?";
        String[] selectionArgs = { cid }; // matched to "?" in selection
        Cursor cursor = database.query(table, columnsToReturn, selection, selectionArgs, null, null, null);
        if (cursor != null)
        {
            if (cursor.moveToFirst())
            {
                result = tDecrypt(cursor.getString(cursor.getColumnIndex("name_first")));
                result += " ";
                result += tDecrypt(cursor.getString(cursor.getColumnIndex("name_second")));
                Log.d("DEBUG","Found a current card.");
            }
            else
            {
                Log.d("getFullName","Card ID not matched");
                return "No such customer!";
            }
        }
        cursor.close();
        return result;
    }


    public boolean verifyCard(String cid)
    {
        // TODO
        return false;
    }


    /**
    * 
    * @param String Customer ID
    * @param int Product ID
    * @return int Points available to customer.
    */
    public int addCustomerPoints(String cid, int pid)
    {
        database = this.getWritableDatabase();
        boolean success = true;
        if(cid == "" || cid == null || pid == 0) return 0;
        int totPoints = 0;
        int points = 0;
        now.setToNow();
        database.beginTransaction(); // This will roll back if success is not set before endTransaction();
        String sql = "SELECT reward FROM rewards WHERE rid = ?";
        SQLiteStatement stmt = database.compileStatement(sql);
        stmt.bindDouble(1, pid);
        points = (int)(long)stmt.simpleQueryForLong();
        sql = "INSERT INTO points(cust_key, points, reward_fk) VALUES(?,?,?)";
        stmt = database.compileStatement(sql);
        stmt.bindString(1, cid);
        stmt.bindDouble(2, points);
        stmt.bindDouble(3, pid);
        try
        {
            stmt.executeInsert();
        }
        catch(Exception e)
        {
            Log.d("addCustomerPoints","Failed to add points.");
            success = false;
        }
       
        if(success == true)
        {
            database.setTransactionSuccessful();
        }
        else
        {
            Log.d("addCustomerPoints","Failed to add points.");
        }
        database.endTransaction();
        totPoints = getTotalForCustomerPoints(cid);
        Log.d("addCustomerPoints",Integer.toString(totPoints));
        exportDatabase(); // Backups yay!
        return totPoints;
    }

    /**
    * Return the reward points allocated to a product
    * @param int The ID of the product
    */
    public int getRewardPoints(int rid)
    {
        database = this.getWritableDatabase();
        //Log.d("getRewardPoints",product);
        int value = 0;
        String table = "rewards";
        String[] columnsToReturn = {"reward"};
        String selection = "rid = ?";
        String[] selectionArgs = { Integer.toString(rid)}; // matched to "?" in selection
        Cursor cursor = database.query(table, columnsToReturn, selection, selectionArgs, null, null, null);
        if (cursor != null)
        {
            if (cursor.moveToFirst())
            {
                value = cursor.getInt(cursor.getColumnIndex("reward"));
            }
        }
        cursor.close();
        return value;
    }

    /**
    * Return the redeem points allocated to a product
    * @param int The ID of the product
    * @return int The points.
    */
    public int getRedeemPoints(int rid)
    {
        database = this.getWritableDatabase();
        //Log.d("getRewardPoints",product);
        int value = 0;
        String table = "rewards";
        String[] columnsToReturn = {"redeem"};
        String selection = "rid = ?";
        String[] selectionArgs = { Integer.toString(rid) }; // matched to "?" in selection
        Cursor cursor = database.query(table, columnsToReturn, selection, selectionArgs, null, null, null);
        if (cursor != null)
        {
            if (cursor.moveToFirst())
            {
                value = cursor.getInt(cursor.getColumnIndex("redeem"));
            }
        }
        cursor.close();
        return value;
    }
    

    /**
    * Return a DB cursor of the rewards table.
    * @return Cursor
    */ 
    public Cursor getRewards()
    {
        database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery("SELECT * FROM rewards WHERE valid = 1 ORDER BY ordering;", null);
        if (cursor != null) return cursor;
        else
        {
            cursor.close();
            return null;
        }
    }


    /**
    * Return the most valuable reward the customer has sufficient points for.
    * @param String ID of customer
    * @return String Name of the reward.
    */
    public String getMostValuableReward(String cid)
    {
        String reward = "";
        int cPoints = getTotalForCustomerPoints(cid);
        database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery("SELECT rid, product, redeem  FROM rewards WHERE valid = 1 AND redeem <= ? ORDER BY redeem DESC LIMIT 1;", new String[]{Integer.toString(cPoints)});
        if (cursor != null && cursor.moveToFirst())
        {
            reward = cursor.getString(cursor.getColumnIndex("product"));
        }
        cursor.close();
        return reward;
    }


    public int getCustomerCount()
    {
        database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery("SELECT COUNT(*) FROM customers;", null);
        if (cursor != null && cursor.moveToFirst())
        {
            return cursor.getInt(cursor.getColumnIndex("COUNT(*)"));
        }
        cursor.close();
        return 0;
    }

    public String getPasswordHash(String user)
    {
        database = this.getWritableDatabase();
        //Log.d("getPasswordHash: user",user);
        String sql = "SELECT passhash from password WHERE username = ?";
        Cursor  cursor = database.rawQuery(sql, new String[]{user});
        if (cursor != null && cursor.moveToFirst())
        {
            return cursor.getString(cursor.getColumnIndex("passhash")).toString();
        }
        cursor.close();
        return null;
    }


    public void setPasswordHash(String user, String passhash)
    {
        database = this.getWritableDatabase();
        String sql = "UPDATE password SET passhash = ? WHERE username = ?";
        SQLiteStatement stmt = database.compileStatement(sql);
        stmt.bindString(2, user);
        stmt.bindString(1, passhash);
        //Log.d("setPasswordHash", stmt.toString());
        stmt.execute();
    }

    
    /*************************************************************************
    * Products are never deleted just marked as invalid.
    * @param int Product ID
    */
    public void deleteProduct(int pid)
    {
        database = this.getWritableDatabase();
        String[] selectionArgs = {Integer.toString(pid)};
        String sql = "UPDATE rewards SET valid = 0 WHERE rid = ?";
        SQLiteStatement stmt = database.compileStatement(sql);
        stmt.bindDouble(1, pid);
        stmt.execute();
        exportDatabase();
    }


    public void updateProduct(int ordering, String product, int reward, int redeem, int rid) // TODO...
    {
        database = this.getWritableDatabase();
        String sql = "UPDATE rewards SET ordering = ?, product = ?, reward = ?, redeem = ? WHERE rid = ?";
        SQLiteStatement stmt = database.compileStatement(sql);
        stmt.bindDouble(1, ordering);
        stmt.bindString(2, product);
        stmt.bindDouble(3, reward);
        stmt.bindDouble(4, redeem);
        stmt.bindDouble(5, rid);
        stmt.execute();
        exportDatabase();
    }


/******************************************************************************
Customer claims their loyalty prize, record it.
@param integer product ID
@param String Customer ID
@return Integer Product points

*/
    public int redeemPoints(int prod_id, String cid)
    {
        database = this.getWritableDatabase();
        boolean success = true;
        SQLiteStatement stmt;
        database.beginTransaction(); // This will roll back if success is not set before
        int custPoints = getTotalForCustomerPoints(cid);
        int product_points = getRedeemPoints(prod_id);
        if (product_points > custPoints) return 0; // Not enough points to redeem product.
        Log.d("redeemPoints",cid);
        String sql = "INSERT INTO redemptions(customer_id, product_id, points) VALUES(?,?,?)"; // Timestamp is added by database
        try
        {
            stmt = database.compileStatement(sql);
            stmt.bindDouble(2, prod_id);
            stmt.bindString(1, cid);
            stmt.bindDouble(3, product_points);
            stmt.executeInsert();
        }
        catch(Exception e)
        {
            success = false;
        }
        
       
        Cursor cursor = database.rawQuery("SELECT * from points WHERE redeemed < points AND cust_key = ? ORDER BY pid", new String[]{cid});
        int points = 0;
        int redeemed = 0;
        int pid = 0;
        if (cursor != null && cursor.moveToFirst())
        {
            while (product_points > 0)
            {
                points = cursor.getInt(cursor.getColumnIndex("points"));
                redeemed = cursor.getInt(cursor.getColumnIndex("redeemed"));
                pid = cursor.getInt(cursor.getColumnIndex("pid"));
                // Subtract 
                product_points -= (points - redeemed);
                Log.d("product_points: ",Integer.toString(product_points));
                //if (product_points < 0) break;
                try
                {
                    sql = "UPDATE points SET redeemed = points WHERE pid = ?";
                    stmt = database.compileStatement(sql);
                    stmt.bindDouble(1, pid);
                    stmt.execute();
                }
                catch(Exception e)
                {
                    success = false;
                }
                cursor.moveToNext();
            }
            if (product_points < 0) // Still some residual points
            {
                try
                {
                    sql = "UPDATE points SET redeemed = ? WHERE pid = ?";
                    stmt = database.compileStatement(sql);
                    stmt.bindDouble(1, -product_points);
                    stmt.bindDouble(2, pid);
                    stmt.execute();
                }
                catch(Exception e)
                {
                    success = false;
                }
            }
        }
        if(success == true)
        {
            database.setTransactionSuccessful();
        }
        else
        {
            Log.d("addCustomerPoints","Failed to add points.");
        }
        database.endTransaction();
        exportDatabase();
        return getRedeemPoints(prod_id);
    }



    /****************************************************************************
    Get to total points awarded minus the total redeemed.
    */
    public int getTotalForCustomerPoints(String cid)
    {
        {
            int points = 0;
            database = this.getWritableDatabase();
            Cursor cursor = database.rawQuery(
            "SELECT SUM(points - redeemed) AS points FROM points WHERE cust_key = ? AND timestamp >= DATETIME('now','-" + getExpireDays() + " days')", new String[]{cid});
            if (cursor != null && cursor.moveToFirst())
                {
                    points = cursor.getInt(cursor.getColumnIndex("points"));
                }
        cursor.close();  
        return points;
        }
    }


    /*
    Get the number of days that points expire.
    */
    public int getExpireDays()
    {
        int expd = 0;
        Cursor cursor = database.rawQuery("SELECT * from key_value WHERE key = 'expiredays'", null);
        if (cursor != null && cursor.moveToFirst())
        {
            String value = cursor.getString(cursor.getColumnIndex("value"));
            String value_type = cursor.getString(cursor.getColumnIndex("value_type"));
            if(value_type.equals("INTEGER"))expd =  Integer.parseInt(value);
        }
        cursor.close();
        return expd;
    }


    public String timeToString(Time now)
    {
        String datetime = Integer.toString(now.year) +
            "-" +  String.format("%02d", now.month + 1 ) +
            "-" +  String.format("%02d", now.monthDay) +
            "T" +  String.format("%02d", now.hour) +
            ":" +  String.format("%02d", now.minute) +
            ":" +  String.format("%02d", now.second);
        return datetime;
    }


    /***************************************************************************
    * Export the tables as CSV files.
    */

    public String exportAllCSV()
    {
        exportCSV("rewards");
        exportCSV("customers");
        exportCSV("points");
        exportCSV("redemptions");
        return "Tables exported to /sdcard/*.csv";
    }


    /***************************************************************************
    * Export the data in a for useful for accounting and sales analysis.
    */
    private void exportCSV(String table)
    {
        database = this.getWritableDatabase();
        File exportDir = new File(Environment.getExternalStorageDirectory(), "");
        if (!exportDir.exists())
        {
            exportDir.mkdirs();
        }

        File file = new File(exportDir, table +".csv");
        try
        {
            file.createNewFile();
            CSVWriter csvWrite = new CSVWriter(new FileWriter(file));
            Cursor curCSV = database.rawQuery("SELECT * FROM " + table ,null);
            csvWrite.writeNext(curCSV.getColumnNames());
            int colCount = curCSV.getColumnCount();
            while(curCSV.moveToNext())
            {
                String arrStr[] =  new String[colCount];
                for (int i = 0; i < colCount; i++)
                {
                    arrStr[i] = curCSV.getString(i); // Would like to have mixed values but...
                }
                csvWrite.writeNext(arrStr);
            }
            csvWrite.close();
            curCSV.close();
        }
        catch(Exception sqlEx)
        {
            Log.e("exportCSV", sqlEx.getMessage(), sqlEx);
        }

    }


    public String exportDatabase()
    {
        try
        {
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();
            if (sd.canWrite())
            {
                String currentDBPath = "/data/data/com.mutant.loyalty/databases/loyalty.db";
                String backupDBPath = "loyalty.db";
                File currentDB = new File(currentDBPath);
                File backupDB = new File(sd, backupDBPath);
                if (currentDB.exists())
                {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }
            return "Database saved to /sdcard/loyalty.db";
        }
        catch (Exception e)
        {
            Log.e("exportDatabase", e.getMessage(), e);
            return "Oops database not exported!";
        }
    }


/******************************************************************************
Export sales and redemptions in a for useful for accounting and sales analysis.
TODO: Export redemptions sales in a suitable format.
*/
    public String exportSales()
    {
        database = this.getWritableDatabase();
        CSVWriter csvWrite = null;
        File exportDir = new File(Environment.getExternalStorageDirectory(), "");
        if (!exportDir.exists())
        {
            exportDir.mkdirs();
        }

        File file = new File(exportDir, "sales.csv");
        try
        {
            file.createNewFile();
            csvWrite = new CSVWriter(new FileWriter(file));
        }
        catch(IOException ioe)
        {
            Log.e("exportSales", ioe.getMessage(), ioe);
        }

        // Do the customer purchas points.
         csvWrite.writeNext(new String[] {"Loyalty sales"});
         try
        {
            Cursor curCSV = database.rawQuery("SELECT points.timestamp, name_first,  name_second, product  FROM customers, rewards, points WHERE points.cust_key=customers.cid AND rewards.rid=points.reward_fk;" ,null);
            csvWrite.writeNext(curCSV.getColumnNames());
            int colCount = curCSV.getColumnCount();
            while(curCSV.moveToNext())
            {
                String arrStr[] =  new String[colCount];
                arrStr[0] = curCSV.getString(0);
                // I'm of the opinion that names should not be attached to purchases, even though a customers identity can be inferred by observed behaviour.
                //arrStr[1] = tDecrypt(curCSV.getString(1));
                //arrStr[2] = tDecrypt(curCSV.getString(2));
                arrStr[3] = curCSV.getString(3);
                csvWrite.writeNext(arrStr);
            }
            //csvWrite.close();
            csvWrite.writeNext(new String[]{""});
            csvWrite.writeNext(new String[]{""});
            curCSV.close();
        }
        catch(Exception sqlEx)
        {
            Log.e("exportCSV", sqlEx.getMessage(), sqlEx);
        }


        // Now do the redemptions
        csvWrite.writeNext(new String[]{"Loyalty redemptions"});
        try
        {
            //CSVWriter csvWrite = new CSVWriter(new FileWriter(file));
            Cursor curCSV = database.rawQuery("SELECT redemptions.timestamp, redemptions.points, rewards.product FROM redemptions, rewards WHERE redemptions.timestamp > '2015-12-01' AND rewards.rid = redemptions.product_id", null);
            csvWrite.writeNext(curCSV.getColumnNames());
            int colCount = curCSV.getColumnCount();
            while(curCSV.moveToNext())
            {
                String arrStr[] =  new String[colCount];
                arrStr[0] = curCSV.getString(0);
                arrStr[1] = curCSV.getString(1);
                arrStr[2] = curCSV.getString(2);
                csvWrite.writeNext(arrStr);
            }
            csvWrite.close();
            curCSV.close();
        }
        catch(Exception sqlEx)
        {
            Log.e("exportCSV", sqlEx.getMessage(), sqlEx);
        }
        return "Sales exported to /sdcard/sales.csv.";
    }


    /***************************************************************************
    SHA1 Encryption od String
    */
    public static String hashPassword(String password)
    {
        String sha1 = "";
        try
        {
            MessageDigest crypt = MessageDigest.getInstance("SHA-1");
            crypt.reset();
            crypt.update(password.getBytes("UTF-8"));
            sha1 = byteToHex(crypt.digest());
        }
        catch(NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        catch(UnsupportedEncodingException e)
        {
            e.printStackTrace();
        }
        return sha1.toString();
    }

    public static String byteToHex(final byte[] hash)
    {
        Formatter formatter = new Formatter();
        for (byte b : hash)
        {
            formatter.format("%02x", b);
        }
        String result = formatter.toString();
        formatter.close();
        return result;
    }


    /*****************************************************************************
    * Trivial 'encryption' using Base64.  This is merely to prevent casual cheating the system and is
    * not intended to provide protection against cracking by a determined cheat.
    */
    public String tEncrypt(String s)
    {
        return Base64.encodeToString(s.getBytes(), Base64.DEFAULT) ;
    }

    public String tDecrypt(String s)
    {
        return new String( Base64.decode(s, Base64.DEFAULT));
    }



} // END CLASS
