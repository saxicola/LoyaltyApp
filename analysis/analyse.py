#!/bin/env python2
# -*- coding: utf-8 -*- 

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import csv
import os, sys
import sqlite3
import base64
from collections import Counter
import locale



try: dbfile = sys.argv[1]
except:
    print "Useage: " + sys.argv[0] + " database_file_name"
    quit(1)
print dbfile

csvfile = "data/eagle_sales.csv"
db_file = "data/eagle.db"


class Analyse(object):
    def __init__(self, dbfile):
        self.conn = self.openDB(dbfile)
        return

    def openDB(self, dbfile):
        conn = sqlite3.connect(dbfile)
        return conn

    def getDecodedUsers(self):
        sql = "SELECT * from customers"
        cur = self.conn.cursor()
        cur.execute(sql)
        customers = cur.fetchall()
        i = 1
        for cust in customers:
            print i, cust[0],base64.b64decode(cust[2]),base64.b64decode(cust[3])
            i += 1
        return

    def decode(self, coded):
        return base64.b64decode(coded)

    def getSalesHisto(self):
        sql = "SELECT points.timestamp, product \
                FROM rewards, points \
                WHERE rewards.rid=points.reward_fk \
                AND  points.timestamp > '2015-12-01' \
                ORDER  BY points.timestamp"
        cur = self.conn.cursor()
        cur.execute(sql)
        sales = cur.fetchall()
        sales_dict = {}
        beverage = []
        for sale in sales:
            #print sale[0].encode('ascii','ignore'), sale[1].encode('ascii','ignore')
            beverage.append(sale[1].encode('ascii','ignore'))
        return Counter(beverage)


    def getSalesHistory(self):
        sql = "SELECT points.timestamp, product, name_first, name_second, points.points \
        FROM points, rewards, customers \
        WHERE points.timestamp >= '2015-12-01' \
        AND customers.cid = points.cust_key \
        AND reward_fk = rewards.rid  \
        ORDER BY points.timestamp"
        cur = self.conn.cursor()
        cur.execute(sql)
        sales = cur.fetchall()
        print "DATE\tPRODUCT\tCUSTOMER\tPOINTS"
        for sale in sales:
            print sale[0], "\t", sale[1], "\t",self.decode(sale[2]), self.decode(sale[3]), "\t",sale[4], "\t1"
        return sales
        

    def makeSalesHistory(self, sales):
        ''' FIXME '''
        saless = sales.most_common()
        print saless
        x_val = [x[0] for x in saless]
        y_val = [x[1] for x in saless]
        print y_val
        # We need percentages of numbers
        y_sum = sum(y_val)
        p_val = [x*100/y_sum for x in y_val]
        #print p_val
        #quit(0)
        X = np.arange(len(sales))
        plt.bar(X, p_val, align='center', width=0.5)
        plt.ylim(0,max(p_val)+5)
        plt.subplots_adjust(bottom=0.2)
        plt.xticks(X, x_val , rotation = -90 )
        plt.xlabel('Item')
        plt.ylabel('% Sales')
        #ymax = max(y_val) + 1
        #plt.ylim(0, ymax)
        plt.title("% Sales by NUmber")
        plt.show()


    def makeSalesValueBar(self, sales_values):
        ''' TODO '''
        saless = sales.most_common()
        print saless
        x_val = [x[0] for x in saless]
        y_val = [x[1] for x in saless]
        print y_val
        # We need percentages of value
        y_sum = sum(y_val)
        p_val = [x*100/y_sum for x in y_val]
        X = np.arange(len(sales))
        plt.bar(X, p_val, align='center', width=0.5)
        plt.ylim(0,max(p_val)+5)
        plt.subplots_adjust(bottom=0.2)
        plt.xticks(X, x_val , rotation = -90 )
        plt.title("% Sales by Number")
        plt.show()


    def makeRedemption_report(self):
        ''' Make a printable report of redemptions for accounting purposes. '''
        total = 0
        sql = "SELECT  redemptions.timestamp,  points, product, name_first, name_second \
                FROM redemptions, rewards, customers \
                WHERE redemptions.timestamp > '2015-12-01'\
                AND redemptions.timestamp < '2016-03-20' \
                AND redemptions.product_id  = rewards.rid \
                AND redemptions.customer_id = customers.cid \
                ORDER BY redemptions.timestamp"
        cur = self.conn.cursor()
        cur.execute(sql)
        rewards = cur.fetchall()
        print "DATE\tCUSTOMER\tPOINTS\tPRODUCT"
        for reward in rewards:
            total += reward[1]/100.0
            print reward[0], "\t", self.decode(reward[3]), self.decode(reward[4]),"\t", locale.currency(reward[1]/100.0), "\t", reward[2]        
        print "Total rewards: " , locale.currency(total)
        return rewards

    def timeout_test(self):
        ''' Make sure we get the correct points no matter what timeout is set
        Use Rob's ID for this as he has the longest history and isn't me.
        '''
        UID = "0xa5540a3b" # Rob Ale Williams
        UID = "0xdcdf083b" # Mike Evans
        #UID = "0xc70e123b" # Ali
        timeouts = (1000, 100, 50, 25)
        redemptions = []
        purchases = []
        points = []
        users = []
        usersSQl = "SELECT cid, name_first, name_second from customers"
        cur = self.conn.cursor()
        cur.execute(usersSQl)
        users = cur.fetchall()
        #for user in users:
            #print user[0]

        for t in timeouts:
            redemptionsSQL = "SELECT SUM(points) FROM redemptions \
            WHERE customer_id = '" + UID + "' \
            AND timestamp >= datetime('now', '-" + str(t) + " days')"
            #cur = self.conn.cursor()
            cur.execute(redemptionsSQL)
            redemptions.append(cur.fetchone()[0])
            
            purchasesSQL = "SELECT SUM(points) FROM points \
            WHERE cust_key = '" + UID + "' \
            AND timestamp >= datetime('now', '-" + str(t) + " days')"
            cur.execute(purchasesSQL)
            purchases.append(cur.fetchone()[0])
        
        print purchases
        print redemptions
        for i in range(0,len(purchases)):
            points.append(purchases[i] - redemptions[i])
            #print purchases[i] - redemptions[i]
        print points
        return

    def mark_as_redeemed(self):
        ''' Mark all sales as redeemed where there is a corresponding redemption.
        The would mean having an adjustment because a sale may not be completely
        redeemed. Yikes!
        '''
        # First add our new column
        sql = "alter table points add redeemed INTEGER DEFAULT 0"
        try:
            cur.execute(sql)
            self.conn.commit()
        except:
            pass
        userdata = []
        purchases = None
        points = None
        users = None
        usersSQl = "SELECT cid, name_first, name_second from customers"
        cur = self.conn.cursor()
        cur.execute(usersSQl)
        self.conn.commit()
        users = cur.fetchall()
        for user in users:
            #print user[0]
            sql = "SELECT * FROM redemptions WHERE customer_id =  '" + user[0] + "' ORDER BY customer_id, timestamp"
            cur.execute(sql)
            data = cur.fetchall()
            userdata.append(data)
            #print userdata
        #quit(0)
        for user_redemptions in userdata:
            if len(user_redemptions) == 0: continue
            #print user_redemptions
            for redeem in user_redemptions:
                if len(redeem) == 0:
                    continue
                    rolling_tot = 0
                remainder = 0
                redemption = redeem[4]
                print redemption
                
                print "\nProcessing redemption: ", redeem
                # For each redemption, for each customer, get the earlier sales if not already
                # processed as redeemed: so redeemed < points
                sql = "SELECT * from points \
                WHERE points.cust_key = '" + redeem[1] + "' \
                AND points.timestamp < '" + redeem[3] + "' \
                AND points.points > points.redeemed \
                ORDER BY timestamp"
                cur.execute(sql)
                unredeemed = cur.fetchall()
                #print type(unredeemed)
                #print unredeemed
                #break # TEST
                # Now for each sale test if it's been redeemed.
                for sale in unredeemed:
                    print sale
                    pid = sale[0]
                    timestamp = sale[1]
                    cust_key = sale[2]
                    redeemed = sale[5]
                    points = sale[3]
                    print pid, points, redeemed, redemption
                    redemption -= (points - redeemed)
                    if redemption <= 0:
                        sql = "UPDATE points SET redeemed = '" + str(abs(redemption)) + "' WHERE pid = '" + str(pid) +"'"
                        cur.execute(sql)
                        print sql
                        break # We're done
                    sql = "UPDATE points SET redeemed = points.points WHERE pid = '" + str(pid) +"'"
                    print sql
                    cur.execute(sql)
                    
        self.conn.commit()
        sql = "select sum(points) from redemptions WHERE timestamp >= '2015-12-01' AND timestamp < '2016-03-20';"
        cur.execute(sql)
        r1 = cur.fetchone()[0]

        sql = "select sum(redeemed)  from points WHERE timestamp >= '2015-12-01' AND timestamp < '2016-03-20';"
        cur.execute(sql)
        r2 = cur.fetchone()[0]
        print "These next two values should match."
        print "Redemptions table: ",r1, ". Sales table :",r2 # Should be the same
                    
            
        


        
def print_help():
    print "Useage: analyse.py <database_file> <analysis type>"
    print "0: Print a list of users and their card numbers"
    print "1: Run a test to ensure data are consistent for various timeout periods"
    print "2: Print a sales history, best redirected to a file with > sales.csv"
    print "3: Create a histogram of sales by product, with a test printout"
    print "4: Print a redemption report, best redirected to a file with > redemptions.csv"
    print "5: "
    print "6: "
    print "7: "
    print "8: "
    print "9: "
    return

if __name__ == "__main__":
    #print len(sys.argv)
    if len(sys.argv) < 3:
        print_help()
        quit(0)
    app = Analyse(dbfile)
    if sys.argv[2] == "0":
        app.getDecodedUsers()
    elif sys.argv[2] == "1":
        app.timeout_test()
    elif sys.argv[2] == "2":
        sales = app.getSalesHistory()
        print sales
    elif sys.argv[2] == "3":
        sales = app.getSalesHisto()
        app.makeSalesHistory(sales)
        print sales
    elif sys.argv[2] == "4":
        app.makeRedemption_report()
    # This can only be run once with any result
    elif sys.argv[2] == "9":
        app.mark_as_redeemed()
    app.conn.close()

